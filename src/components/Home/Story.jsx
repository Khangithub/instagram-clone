import React, {useContext} from 'react';
import './Story.css';
import {useHistory} from 'react-router-dom';
import {StoryContext} from '../../providers/StoryContextProvider';

function Story({avatar, username, storyIndex}) {
  const history = useHistory();
  const {setCurrentStoryIndex} = useContext(StoryContext);
  return (
    <div
      className="story"
      onClick={() => {
        setCurrentStoryIndex({
          main: parseInt(storyIndex),
          sub: 0,
        });
        history.push(`/stories/${storyIndex}`);
      }}
    >
      <div className="story__avatar__container">
        <img src={avatar} alt="avatar" draggable={false} />
      </div>
      <p>{username}</p>
    </div>
  );
}

export default Story;
