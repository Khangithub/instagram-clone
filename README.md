**Git repo: https://gitlab.com/Khangithub/instagram-clone**

# 2/2/2021

## 1) Sửa layout

- fixed sidebar và header để khi scroll xuống sẽ không biến mất

- responsive cho các màn hình di động và ipad

- Cập nhật lại các hình ảnh và asset

### `trạng thái`: hoàn tất
## 2) Chức năng like post bằng cách bấm nút hình trái tim

- khi người dùng bấm vào nút hình trái tim viền đen, trái tim biến thành màu đỏ

- số lượng người like sẽ được cập nhật lại

- Mỗi người chỉ được like một lần

### `trạng thái`: hoàn tất

# 3/2/2021
## 1) Chức năng unlike post 
- khi người dùng bấm vào nút hình trái tim màu đỏ, trái tim biến thành viền đen

- uid của người đó sẽ bị xóa khỏi danh sách những người like post

- danh sách những người like post sẽ được cập nhật lại
### `trạng thái:` hoàn tất
## 2) Chức năng like post bằng double click vào ảnh của post

- khi người dùng double click vào một tấm hình, nếu post chứa tấm hình đó chưa có người like, post đó sẽ được like

- nút viền trái tim đen sẽ biến thành nút màu đỏ

- Mỗi lần double click sẽ chỉ người dùng sẽ chỉ được like một lần

### `trạng thái:` hoàn tất

## 3) Refactor code
- chia lại module

- thêm css

- sử lý asynce await bất đồng bộ

### `trạng thái`: tiếp tục

# 4/2/2021
## 1) Viết protect route
### `trạng thái`: hoàn tất

## 2) Tạo layout cho route story
- tạo layout cho route story để xem các story của người dùng

- mock data để hiển thị các story
### `trạng thái`: tiếp tục

# 5/2/2021
## 1) map dữ liệu và thêm css cho story 
### `trạng thái`: tiếp tục

## 2) thêm animation cho story
- khi chuyển story thì thêm hiệu ứng scale và fade cho story đang xem vào 3s đâù
### `trạng thái`: hoàn thành

# 10/2/2021
## Sử lý bất đồng bộ của useState trong việc chuyển next và previous các story
- khi chuyển giữa các story thì ngăn không cho story chuyển về story cũ
### `trạng thái`: tiếp tục

# 11/2/2021
## thêm cuộc khảo sát nếu story là dạng ảnh
### `trạng thái`: hoàn thành

# 16/2/2021
## thêm chức năng chỉnh sửa post
- nếu người dùng tạo ra post, họ có quyền sửa caption và ảnh/video của post đó
- tạo modal ở phía frontend
- viết api ở phía backend
### `trạng thái`: hoàn thành

# 18/2/2021
## chức năng xem toàn bộ danh sách comment của một post cụ thể
- khi click vào một post, toàn bộ danh sách comment sẽ được hiển thị ra
### `trạng thái`: tiếp tục

## thêm emoji vào comment và caption của post
- người dùng có thể thêm emoji vào comment và caption của post đó
### `trạng thái`: hoàn thành

## tái sử dụng module và refractor code
### `trạng thái`: tiếp tục

