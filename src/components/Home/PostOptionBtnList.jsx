import React, {useContext, useState} from 'react';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';

import './PostOptionBtnList.css';
import {DeleteIcon, EditIcon} from '../../assets/imgs';
import {PostListContext} from '../../providers/PostListContextProvider';
import {CurrentUserContext} from '../../providers/CurrentUserContextProvider';
import {Dropdown} from 'react-bootstrap';
import EditPostModal from './PostModals/EditPostModal';

export default function PostOptionBtnList({data, id}) {
  const {setPostList, setPostListLoading} = useContext(PostListContext);
  const {currentUser} = useContext(CurrentUserContext);
  const [modalShow, setModalShow] = useState(false);

  const handleDeletePost = async () => {
    try {
      const DeletePostRes = await fetch(
        `https://asia-east2-instagram-clone-86a6e.cloudfunctions.net/api/post/${id}`,
        {
          method: 'DELETE',
          headers: {
            'content-type': 'application/json; charset=UTF-8',
          },
        }
      );

      const DeletePostJson = await DeletePostRes.json();

      if (DeletePostJson) {
        try {
          const getPostListRes = await fetch(
            `https://asia-east2-instagram-clone-86a6e.cloudfunctions.net/api/post`
          );

          const getPostListJson = await getPostListRes.json();

          if (getPostListJson) {
            setPostList(getPostListJson.postList);
            setPostListLoading(false);
          } else {
            console.error(
              getPostListJson + 'error in getPostListJson after deletePost'
            );
          }
        } catch (err) {
          console.error(
            'err in trycatch block of GetPostList' + JSON.stringify(err)
          );
        }

        alert(DeletePostJson.message);
      } else {
        console.error(DeletePostJson + 'error in DeletePostJson');
      }
    } catch (err) {
      console.error(JSON.stringify(err) + ' err in trycatch PostOptionBtnList');
    }
  };

  return (
    currentUser?.uid === data?.uid && (
      <Dropdown className="delete__post__btn">
        <Dropdown.Toggle variant="light">
          <MoreHorizIcon fontSize="small" />
        </Dropdown.Toggle>

        <Dropdown.Menu>
          <Dropdown.Item onClick={handleDeletePost}>
            <img src={DeleteIcon} alt="" />
            <span>Delete Post</span>
          </Dropdown.Item>

          <Dropdown.Item onClick={() => setModalShow(true)}>
            <img src={EditIcon} alt="" />
            <span>Edit Post</span>
          </Dropdown.Item>

          <EditPostModal
            show={modalShow}
            setModalShow={setModalShow}
            data={data}
            id={id}
            onHide={() => setModalShow(false)}
          />
        </Dropdown.Menu>
      </Dropdown>
    )
  );
}


