import React, {useEffect, useMemo, useState} from 'react';
import Cookies from 'universal-cookie';
import {auth, provider} from '../firebase';
import {useHistory} from 'react-router-dom';

export const CurrentUserContext = React.createContext();

export default function CurrentUserContextProvider(props) {
  const cookies = useMemo(() => {
    return new Cookies();
  }, []);

  const history = useHistory();
  const [currentUser, setUser] = useState(null);

  useEffect(() => {
    let currentUser = cookies.get('currentUser');
    return currentUser ? setUser(currentUser) : setUser(null);
  }, [cookies]);

  const getCurrentUser = () => {
    const cookies = new Cookies();
    const currentUser = cookies.get('currentUser');
    return currentUser;
  };

  const signIn = () =>
    auth
      .signInWithPopup(provider)
      .then((doc) => {
        const {url} = doc.additionalUserInfo.profile.picture.data;
        const {uid, displayName, phoneNumber, email} = doc.user;

        cookies.set(
          'currentUser',
          JSON.stringify({
            url,
            uid,
            displayName,
            phoneNumber,
            email,
          })
        );

        history.push('/');
      })
      .catch((err) => alert(err.message));

  const signOut = () =>
    auth
      .signOut()
      .then((doc) => {
        console.log(doc, 'sign out');
        history.push('/signin');
        cookies.remove('currentUser');
      })
      .catch((err) => alert(err.message));

  return (
    <CurrentUserContext.Provider
      value={{currentUser, setUser, signIn, signOut, getCurrentUser}}
    >
      {props.children}
    </CurrentUserContext.Provider>
  );
}
