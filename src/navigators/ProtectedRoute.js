import React, {useContext} from 'react';
import {CurrentUserContext} from '../providers/CurrentUserContextProvider';

import {Redirect, Route} from 'react-router-dom';

export default function ProtectedRoute({component: Component, ...rest}) {
  const {getCurrentUser} = useContext(CurrentUserContext);
  const currentUser = getCurrentUser();

  return (
    <Route
      {...rest}
      render={(props) => {
        if (currentUser) {
          return <Component {...props} />;
        } else {
          return (
            <Redirect
              to={{
                pathname: '/signin',
                state: {
                  from: props.location,
                },
              }}
            />
          );
        }
      }}
    />
  );
}
