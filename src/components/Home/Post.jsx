import React, {useState, useContext} from 'react';
import './Post.css';
import 'react-responsive-carousel/lib/styles/carousel.min.css'; // requires a loader
import {Carousel} from 'react-responsive-carousel';
import {IconButton} from '@material-ui/core';
import {
  RedHeartIcon,
  MessageIcon,
  SendIcon,
  HeartIcon,
} from '../../assets/imgs';
import {useHistory, useLocation} from 'react-router-dom';

import prettyMilliseconds from 'pretty-ms';
import PostOptionBtnList from './PostOptionBtnList';
import LikeButton from './LikeButton';
import {PostListContext} from '../../providers/PostListContextProvider';
import {CurrentUserContext} from '../../providers/CurrentUserContextProvider';
import CreateCommentModal from '../Home/PostModals/CreateCommentModal';

function Post({data, id}) {
  const history = useHistory();
  const {pathname} = useLocation();
  var createPostTimestamp = new Date(data.createAt).getTime();
  var timeDiff = Date.now() - createPostTimestamp;

  const [seeMoreCaption, setMoreCaption] = useState(true);
  const [seeMoreComment, setMoreComment] = useState(true);
  const [likePostByDBClick, setLikePostByDBClick] = useState(false);
  const {likePost} = useContext(PostListContext);
  const [comment, setComment] = useState('');
  const [commentList, setCommentList] = useState(data.commentList);
  const {getCurrentUser} = useContext(CurrentUserContext);
  const {displayName, uid} = getCurrentUser();

  const truncateText = (caption, pivot, type) => {
    if (type === 'caption') {
      if (caption.length < pivot) {
        return caption;
      }

      let copiedDescription = ''.concat(caption);
      let firstPart = copiedDescription.slice(0, pivot);
      let text = seeMoreCaption ? firstPart : copiedDescription;
      let spanName = seeMoreCaption ? 'Thêm' : 'Thu gọn';

      return (
        <>
          <span>{text}</span>
          <span
            onClick={() => setMoreCaption(!seeMoreCaption)}
            className="post__caption__truncate__btn"
          >
            &nbsp;&nbsp;...&nbsp;{spanName}
          </span>
        </>
      );
    } else {
      if (caption.length < pivot) {
        return caption;
      }

      let copiedDescription = ''.concat(caption);
      let firstPart = copiedDescription.slice(0, pivot);
      let text = seeMoreComment ? firstPart : copiedDescription;
      let spanName = seeMoreComment ? 'Thêm' : 'Thu gọn';

      return (
        <>
          <span>{text}</span>
          <span
            onClick={() => setMoreComment(!seeMoreComment)}
            className="post__caption__truncate__btn"
          >
            &nbsp;&nbsp;...&nbsp;{spanName}
          </span>
        </>
      );
    }
  };

  const handleDeleteComment = (_i) => {
    return async (e) => {
      e.preventDefault();

      try {
        const DeleteCommentRes = await fetch(
          `https://asia-east2-instagram-clone-86a6e.cloudfunctions.net/api/comment/${id}`,
          {
            method: 'DELETE',
            headers: {
              'content-type': 'application/json; charset=UTF-8',
            },

            body: JSON.stringify({index: _i}),
          }
        );

        const DeleteCommentJson = await DeleteCommentRes.json();

        if (DeleteCommentJson.updatedCommentList) {
          console.log(DeleteCommentJson.updatedCommentList, 'deleted');
          setCommentList(DeleteCommentJson.updatedCommentList);
        }
      } catch (err) {
        console.error(
          JSON.stringify(err) + ' err in trycatch DeleteCommentRes'
        );
      }
    };
  };

  return (
    <div className="post">
      <div className="post__header__container">
        <div className="post__header__poster__info">
          <img src={data.url} alt="" />

          <span>{data.displayName}</span>
        </div>

        <PostOptionBtnList data={data} id={id} />
      </div>

      <div
        className="post__media__list"
        onDoubleClick={() => {
          setLikePostByDBClick(true);
          if (data.likedList.filter((each) => each.uid === uid).length <= 0) {
            likePost(id, displayName, uid);
          }
          setTimeout(() => setLikePostByDBClick(false), 1000);
        }}
      >
        <Carousel>
          {data.uploadedMediaList.map(({mediaURL, type}, index) => (
            <div key={index}>
              {type.includes('image') && (
                <div
                  className="media"
                  style={{backgroundImage: `url("${mediaURL}")`}}
                ></div>
              )}
              {type.includes('video') && (
                <video className="media" src={mediaURL} controls alt="" />
              )}
            </div>
          ))}
        </Carousel>
      </div>

      {likePostByDBClick && (
        <img className="post__heart__icon" src={RedHeartIcon} alt="" />
      )}

      <div className="post__react__detail">
        <div className="post__option__button__list">
          <div className="post__option__button__list__containter">
            <LikeButton data={data} id={id} />
            <IconButton
              onClick={() =>
                history.push({
                  pathname: `/p/${id}`,
                  state: {
                    data,
                    prevPath: pathname,
                  },
                })
              }
            >
              <img src={MessageIcon} alt="" />
            </IconButton>
            <IconButton>
              <img src={SendIcon} alt="" />
            </IconButton>
          </div>
        </div>

        <p className="post__react__detail__like">
          {data.likedList?.length === 0
            ? `${data.like} lượt thích`
            : `${data.likedList
                .map((each) => each.displayName)
                .join(', ')} và ${data.like} người khác đã thích`}
        </p>

        <div className="post__caption__container">
          <span>{data.displayName}&nbsp;</span>
          <span>{truncateText(data.caption, 200, 'caption')}</span>
        </div>

        <div className="post__comment__list" onHover>
          {commentList
            .map((comment, _i) => (
              <div className="post__comment__container" key={_i}>
                <div
                  className="post__comment__content"
                  style={{pointerEvents: uid === comment.uid ? 'auto' : 'none'}}
                  onClick={handleDeleteComment(_i)}
                >
                  <span>{comment.displayName}&nbsp;</span>
                  <span>{truncateText(comment.comment, 100, 'comment')}</span>
                  <span>❌ click to delete your comment ❌  </span>
                </div>
                {uid !== comment.uid && (
                  <img
                    className="post__comment__like__comment__btn"
                    src={HeartIcon}
                    alt=""
                  />
                )}
              </div>
            ))
            .reverse()}
        </div>

        <p className="post__react__detail__time__diff">
          {prettyMilliseconds(timeDiff, {compact: true})}
        </p>
      </div>
      <CreateCommentModal
        comment={comment}
        setComment={setComment}
        postId={id}
        setCommentList={setCommentList}
      />
    </div>
  );
}

export default Post;
