import React from 'react';
import './Poll.css';

var upvote = Math.floor(Math.random() * Math.floor(100)) + 1;
var downvote = 100 - upvote;

export default function Poll() {
  return (
    <div
      className="poll"
      style={{
        transform: `rotate(-${
          Math.floor(Math.random() * Math.floor(10)) + 1
        }deg)`,
      }}
    >
      <div
        className="poll__vote__container"
        style={{
          width: `${upvote}%`,
          backgroundColor: upvote > downvote && 'lightgrey',
          margin: upvote > downvote && '1px',
          borderTopLeftRadius: upvote > downvote && '14px',
          borderBottomLeftRadius: upvote > downvote && '14px',
        }}
      >
        <h1>👍</h1>
        <h3>{upvote}%</h3>
      </div>
      <div
        className="poll__vote__container"
        style={{
          width: `${downvote}%`,
          backgroundColor: downvote > upvote && 'lightgrey',
          margin: downvote > upvote && '1px',
          borderTopRightRadius: downvote > upvote && '14px',
          borderBottomRightRadius: downvote > upvote && '14px',
        }}
      >
        <h1> 👎 </h1>
        <h3>{downvote}%</h3>
      </div>
    </div>
  );
}

// background: lightgrey;
//     margin: 1px;
//     border-radius: 14px;
