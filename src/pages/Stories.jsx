import React, {useState, useEffect, useContext} from 'react';
import './Stories.css';
import {IconButton} from '@material-ui/core';
import {InstagramIcon, CloseIcon} from '../assets/imgs';
import {useHistory, useParams} from 'react-router-dom';
import {Row, Col} from 'react-bootstrap';
import CurrentStory from '../components/Stories/CurrentStory';
import SubStory from '../components/Stories/SubStory';
import {StoryContext} from '../providers/StoryContextProvider';

export default function Stories() {
  const history = useHistory();
  const {storyIndex} = useParams();
  const {currentStoryIndex, setCurrentStoryIndex, stories} = useContext(
    StoryContext
  );

  useEffect(() => {
    setCurrentStoryIndex({main: parseInt(storyIndex), sub: 0});
  }, [setCurrentStoryIndex, storyIndex]);

  const [userNavigateStory, setUserNavigateStory] = useState(false);

  return (
    <Row className="stories">
      <Col xs={3} sm={4} className="stories__prev__stories__container">
        <IconButton className="stories__btn" onClick={() => history.push('/')}>
          <img src={InstagramIcon} alt="" />
        </IconButton>

        <div className="stories__container">
          {currentStoryIndex.main - 2 >= 0 && (
            <SubStory
              subStory={stories[currentStoryIndex.main - 2]}
              subStoryIndex={currentStoryIndex.main - 2}
            />
          )}

          {currentStoryIndex.main - 1 >= 0 && (
            <SubStory
              subStory={stories[currentStoryIndex.main - 1]}
              subStoryIndex={currentStoryIndex.main - 1}
            />
          )}

          {currentStoryIndex.main > 0 && (
            <button
              onClick={() => {
                setUserNavigateStory(true);
                setTimeout(() => {
                  setUserNavigateStory(false);
                }, 1000);
                setCurrentStoryIndex({
                  sub: 0,
                  main: currentStoryIndex.main - 1,
                });
                return history.push(`/stories/${currentStoryIndex.main - 1}`);
              }}
            >
              ◀
            </button>
          )}
        </div>

        <div></div>
      </Col>
      <Col
        xs={6}
        sm={4}
        className={`current__story__container ${
          userNavigateStory && 'animation_fadeNScale'
        }`}
      >
        <CurrentStory currentStory={stories[currentStoryIndex.main]} />
      </Col>
      <Col xs={3} sm={4} className="stories__next__stories__container">
        <IconButton className="stories__btn" onClick={() => history.goBack()}>
          <img src={CloseIcon} alt="" />
        </IconButton>

        <div className="stories__container">
          {currentStoryIndex.main < stories.length - 1 && (
            <button
              className="stories__navigate__btn"
              onClick={() => {
                setUserNavigateStory(true);
                setTimeout(() => {
                  setUserNavigateStory(false);
                }, 1000);

                setCurrentStoryIndex({
                  sub: 0,
                  main: currentStoryIndex.main + 1,
                });
                return history.push(`/stories/${currentStoryIndex.main + 1}`);
              }}
            >
              ▶
            </button>
          )}
          {currentStoryIndex.main + 1 <= stories.length - 1 && (
            <SubStory
              subStory={stories[currentStoryIndex.main + 1]}
              subStoryIndex={currentStoryIndex.main + 1}
            />
          )}

          {currentStoryIndex.main + 2 <= stories.length - 1 && (
            <SubStory
              subStory={stories[currentStoryIndex.main + 2]}
              subStoryIndex={currentStoryIndex.main + 2}
            />
          )}
        </div>

        <div></div>
      </Col>
    </Row>
  );
}
