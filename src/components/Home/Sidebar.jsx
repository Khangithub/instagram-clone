import React, {useContext} from 'react';
import {CurrentUserContext} from '../../providers/CurrentUserContextProvider';
import './Sidebar.css';
import {namho, vsabyu, lilyb, thuthi, manh, tanbunhin} from '../../assets/imgs';

function Sidebar() {
  const {signOut, getCurrentUser} = useContext(CurrentUserContext);
  const currentUser = getCurrentUser();

  const sugestFriendList = [
    {
      username: 'namho369',
      reason: 'Mới tham gia Instagram',
      url: namho,
    },
    {
      username: 'vsabyu',
      reason: 'Follows you',
      url: vsabyu,
    },
    {
      username: 'lilyb.nguyen',
      reason: 'Có zach.spring.08 + 5 người nữa theo dõi',
      url: lilyb,
    },
    {
      username: 'thuthi_nguyen',
      reason: 'Có lilbuhler + 5 người nữa theo dõi',
      url: thuthi,
    },
    {
      username: 'manh.md2nd.50',
      reason: 'Có r._cecil + 2 người nữa theo dõi',
      url: manh,
    },
    {
      username: 'tanbunhin1212',
      reason: 'Có r.conor12 + 17 người nữa theo dõi',
      url: tanbunhin,
    },
  ];

  return (
    <div className="sidebar">
      <div className="sidebar__nav__account__container">
        <img src={currentUser.url} alt="" />

        <div>
          <p>{currentUser.displayName}</p>
          <p>{currentUser.displayName}</p>
        </div>

        <span onClick={() => signOut()}>Chuyển</span>
      </div>

      <div className="side__suggest__friend__list__container">
        <div>
          <span>Gợi ý cho bạn</span>
          <span>Xem tất cả</span>
        </div>

        {sugestFriendList.map((friend, index) => (
          <div className="sidebar__suggest__friend" key={index}>
            <img src={friend.url} alt="" draggable={false} />

            <div>
              <p>{friend.username}</p>
              <p>{friend.reason}</p>
            </div>

            <span>Theo dõi</span>
          </div>
        ))}
      </div>

      <div className="sidebar__footer">
        <div>
          <span> Giới thiệu</span>
          <span>Trợ giúp</span>
          <span> Báo chí</span>
          <span> API</span>
          <span> Việc làm</span>
          <span> Quyền riêng tư</span>
          <span>Điều khoản</span>
          <span> Vị trí</span>
          <span> Tài khoản liên quan nhất</span>
          <span>Hashtag</span>
          <span>Ngôn ngữ</span>
        </div>

        <div>
          <span>© 2020 INSTAGRAM FROM FACEBOOK</span>
        </div>
      </div>
    </div>
  );
}

export default Sidebar;
