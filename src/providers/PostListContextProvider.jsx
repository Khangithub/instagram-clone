import React, {useEffect, useState} from 'react';
export const PostListContext = React.createContext();

export default function PostListContextProvider(props) {
  const [postList, setPostList] = useState([]);
  let [getPostListLoading, setPostListLoading] = useState(true);
  console.log(postList, 'postList');

  useEffect(() => {
    const fetchPostList = async () => {
      try {
        const getPostListRes = await fetch(
          `https://asia-east2-instagram-clone-86a6e.cloudfunctions.net/api/post`
        );

        const getPostListJson = await getPostListRes.json();

        if (getPostListJson) {
          setPostList(getPostListJson.postList);
          setPostListLoading(false);
        } else {
          alert(getPostListJson + 'alert in getPostListJson');
        }
      } catch (err) {
        console.error(
          'err in trycatch block of GetPostList' + JSON.stringify(err)
        );
      }
    };

    fetchPostList();
  }, []);

  const likePost = async (id, displayName, uid) => {
    try {
      const likePostReq = await fetch(
        `http://localhost:5001/instagram-clone-86a6e/asia-east2/api/post/like/${id}`,
        {
          method: 'PATCH',
          headers: {
            'content-type': 'application/json; charset=UTF-8',
          },
          body: JSON.stringify({
            displayName,
            uid,
          }),
        }
      );

      const likePostJson = await likePostReq.json();

      if (likePostJson.liked) {
        let newPostList = postList.filter((post) =>
          post.id === id
            ? post.data.likedList.push({
                displayName,
                uid,
              })
            : post
        );

        setPostList(newPostList);
      }
    } catch (err) {
      console.error('error while liking post', err);
    }
  };

  const unlikedPost = async (uid, id) => {
    console.log('unlike post');
    try {
      const unlikePostReq = await fetch(
        `http://localhost:5001/instagram-clone-86a6e/asia-east2/api/post/unlike/${id}`,
        {
          method: 'PATCH',
          headers: {
            'content-type': 'application/json; charset=UTF-8',
          },
          body: JSON.stringify({
            uid,
          }),
        }
      );

      const unlikePostJson = await unlikePostReq.json();

      if (unlikePostJson.unliked) {
        let newPostList = postList.map((post) => {
          if (post.id === id) {
            post.data.likedList = unlikePostJson.updatedLikedList;
          }
          return post;
        });

        setPostList(newPostList);
      }
    } catch (err) {
      console.error('error while unliking post', err);
    }
  };
  return (
    <PostListContext.Provider
      value={{
        postList,
        getPostListLoading,
        setPostList,
        setPostListLoading,
        likePost,
        unlikedPost,
      }}
    >
      {props.children}
    </PostListContext.Provider>
  );
}
