const {db} = require('../util/admin');
const FieldValue = require('firebase-admin').firestore.FieldValue;

exports.add__comment = (req, res, next) => {
  const {comment, displayName, url, uid} = req.body;
  const {id} = req.params;

  return db
    .collection('instagram__posts')
    .doc(id)
    .update({
      commentList: FieldValue.arrayUnion({
        comment,
        uid,
        displayName,
        url,
        createAt: new Date().toISOString(),
        likedCommentList: [],
        repliedCommentList: [],
      }),
    })
    .then((doc) => {
      console.log('comment created', comment);
      next();
    })
    .catch((err) => {
      res.status(500).json({err});
      console.log(err);
    });
};

exports.delete__comment = (req, res, next) => {
  const {id} = req.params;
  const {index} = req.body;

  db.collection('instagram__posts')
    .doc(id)
    .onSnapshot((snapshot) => {
      var updatedCommentList = snapshot.data().commentList.filter((__, _i) => {
        return _i !== index;
      });

      db.collection('instagram__posts')
        .doc(id)
        .set({commentList: updatedCommentList}, {merge: true})
        .then((doc) => {
          console.log('comment deleted');
          return res.status(200).json({
            updatedCommentList,
          });
        })
        .catch((err) => {
          res.status(500).json({err});
          console.log(err);
        });
    });
};

exports.return__new__comment__list = (req, res, next) => {
  const {id} = req.params;

  return db
    .collection('instagram__posts')
    .doc(id)
    .onSnapshot((snapshot) => {
      return res.status(200).json({
        updatedCommentList: snapshot.data().commentList,
      });
    });
};
