import React, {useEffect, useState, useContext} from 'react';
import './CurrentStory.css';
import {StoryContext} from '../../providers/StoryContextProvider';
import {IconButton} from '@material-ui/core';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import VolumeUpIcon from '@material-ui/icons/VolumeUp';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import SendIcon from '@material-ui/icons/Send';
import PauseIcon from '@material-ui/icons/Pause';
import VolumeOffIcon from '@material-ui/icons/VolumeOff';
import ReactPlayer from 'react-player';
import Poll from './Poll';

export default function CurrentStory({currentStory}) {
  const {created, username, avatar, mediaList} = currentStory;
  const {
    currentStoryIndex,
    setCurrentStoryIndex,
    stories,
    setStories,
  } = useContext(StoryContext);
  const [pauseStory, setPauseStory] = useState(false);
  const [muteStory, setMuteStory] = useState(false);

  useEffect(() => {
    const progressList = Array.from(document.querySelectorAll('.progress'));

    progressList.forEach((progress) => {
      progress.classList.add('progress__loading__remove');
      progress.classList.remove('progress__loading__passed');
    });

    setPauseStory(false);

    setTimeout(() => {
      progressList.forEach((progress) => {
        progress.classList.remove('progress__loading__remove');
      });
    }, 500);

    const initProgress = progressList[currentStoryIndex.sub];
    initProgress.classList.add('progress__loading__start');

    initProgress.addEventListener(
      'animationend',
      function () {
        initProgress.classList.remove('progress__loading__start');
        initProgress.classList.add('progress__loading__remove');

        setPauseStory(true);

        var {main, sub} = currentStoryIndex;

        if (sub + 1 > mediaList.length - 1) {
          initProgress.classList.remove('progress__loading__start');
          setCurrentStoryIndex({
            main,
            sub: mediaList.length - 1,
          });
          setPauseStory(true);

          progressList.forEach((progress) => {
            progress.classList.add('progress__loading__remove');
            progress.classList.remove('progress__loading__passed');
            progress.classList.remove('progress__loading__start');
          });
        } else {
          setCurrentStoryIndex({
            main,
            sub: sub + 1,
          });
        }
      },
      false
    );
  }, [currentStoryIndex, setCurrentStoryIndex, mediaList, stories, setStories]);

  return (
    <div className="current__story">
      <div className="current__story__header">
        <div className="current__story__progress__container">
          {mediaList.map((media, index) => (
            <div
              key={index}
              style={{
                animationDuration: `${media.duration}s`,
                animationPlayState: `${pauseStory ? 'paused' : 'running'}`,
              }}
              className="progress"
            ></div>
          ))}
        </div>
        <div className="current__story__controls">
          <div className="current__story__controls__user__info">
            <img src={avatar} alt="" />

            <span>{username}</span>

            <span>{created} giờ</span>
          </div>
          <div className="current__story__option__btn__container">
            <IconButton onClick={() => setPauseStory(!pauseStory)}>
              {pauseStory ? (
                <PlayArrowIcon fontSize="small" />
              ) : (
                <PauseIcon fontSize="small" />
              )}
            </IconButton>
            <IconButton onClick={() => setMuteStory(!muteStory)}>
              {muteStory ? (
                <VolumeOffIcon fontSize="small" />
              ) : (
                <VolumeUpIcon fontSize="small" />
              )}
            </IconButton>
            <IconButton>
              <MoreHorizIcon fontSize="small" />
            </IconButton>
          </div>
        </div>
      </div>
      <div className="current__story__body">
        {mediaList[currentStoryIndex.sub].type === 'image' ? (
          <div
            className="current__story__body__image"
            style={{
              backgroundImage: `url("${mediaList[currentStoryIndex.sub].url}")`,
            }}
          >
            {mediaList[currentStoryIndex.sub].hasPoll && <Poll />}
          </div>
        ) : (
          <ReactPlayer
            url={mediaList[currentStoryIndex.sub].url}
            muted={muteStory}
            // controls
            playing={!pauseStory}
          />
        )}
      </div>
      <div className="current__story__footer">
        <input type="text" placeholder={`Trả lời ${username} ... `} />
        <IconButton>
          <SendIcon />
        </IconButton>
      </div>
    </div>
  );
}
