import React, {useContext} from 'react';
import Story from './Story';
import './StoryList.css';
import {Fade} from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css';
import {StoryContext} from '../../providers/StoryContextProvider';

const STORY_CAPACITY = 7;

export default function StoryList() {
  const {stories} = useContext(StoryContext);
  const renderStories = (stories) => {
    var storyContainerList = [];
    var storyContainer = [];

    for (var i = 0; i < stories.length; i++) {
      if (i % STORY_CAPACITY !== 0 || i === 0) {
        storyContainer.push(stories[i]);
      }

      if (i % STORY_CAPACITY === 6) {
        storyContainerList.push(storyContainer);
        storyContainer = [];
        storyContainer.push(stories[i]);
      }
    }

    return storyContainerList.map((stories, containerIndex) => (
      <div className="each-fade" key={containerIndex}>
        {stories.map(({username, avatar}, eachIndex) => (
          <Story
            username={username}
            avatar={avatar}
            key={eachIndex}
            storyIndex={STORY_CAPACITY * containerIndex + eachIndex}
          />
        ))}
      </div>
    ));
  };

  return (
    <div className="story__list slide-container">
      <Fade>{renderStories(stories)}</Fade>
    </div>
  );
}
