import './App.css';
import React from 'react';
import {Switch, Route} from 'react-router-dom';
import ProtectedRoute from './navigators/ProtectedRoute';

import Home from './pages/Home';
import Login from './pages/Login';
import Stories from './pages/Stories';
import Post from './pages/Post';

function App() {
  return (
    <div className="app">
      <Switch>
        <Route exact from="/signin" render={(props) => <Login {...props} />} />
        <ProtectedRoute exact path="/stories/:storyIndex" component={Stories} />
        <ProtectedRoute exact path="/p/:postId" component={Post} />
        <ProtectedRoute exact path="/" component={Home} />
      </Switch>
    </div>
  );
}

export default App;
