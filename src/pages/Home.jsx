import React from 'react';
import './Home.css';

import Header from '../components/Header/Header';
import Sidebar from '../components/Home/Sidebar';
import Feed from '../components/Home/Feed';

function Home() {
  return (
    <div className="home">
      <Header />
      <div className="home__container">
        <Feed />
        <Sidebar />
      </div>
    </div>
  );
}

export default Home;
