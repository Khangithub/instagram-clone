const functions = require('firebase-functions');
const app = require('express')();
var cors = require('cors');

const {
  get__all__post,
  create__post,
  delete__post,
  like__post,
  get__updated__likedList,
  return__like__list,
  unlike__post,
  edit__post,
  get__post__by__id
} = require('./controllers/post');

const {
  add__comment,
  delete__comment,
  return__new__comment__list,
} = require('./controllers/comment');

app.use(cors({origin: true}));

app.get('/post', get__all__post);
app.get('/post/:id', get__post__by__id);
app.post('/post', create__post);
app.patch('/post/:id', edit__post);
app.delete('/post/:id', delete__post);
app.patch('/post/like/:id', like__post);
app.patch('/post/unlike/:id', get__updated__likedList,unlike__post, return__like__list);

app.post('/comment/:id', cors(), add__comment, return__new__comment__list);
app.delete('/comment/:id', cors(), delete__comment, return__new__comment__list);

//http:baseurl/api
exports.api = functions.region('asia-east2').https.onRequest(app);
