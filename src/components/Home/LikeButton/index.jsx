import React, {useContext} from 'react';
import {HeartIcon, RedHeartIcon} from '../../../assets/imgs';
import {IconButton} from '@material-ui/core';
import {PostListContext} from '../../../providers/PostListContextProvider';
import {CurrentUserContext} from '../../../providers/CurrentUserContextProvider';

function LikeButton({data, id}) {
  const {likedList} = data;
  const {likePost, unlikedPost} = useContext(PostListContext);
  const {getCurrentUser} = useContext(CurrentUserContext);
  const {displayName, uid} = getCurrentUser();

  return (
    <IconButton
      onClick={() => {
        console.log(
          likedList.map((each) => each.uid).includes(uid)
            ? unlikedPost(uid, id)
            : likePost(id, displayName, uid)
        );
      }}
    >
      <img
        src={
          likedList.map((each) => each.uid).includes(uid)
            ? RedHeartIcon
            : HeartIcon
        }
        alt=""
      />
    </IconButton>
  );
}

export default LikeButton;
