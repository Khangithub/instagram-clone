import React, {useState, useContext} from 'react';
import './CreatePostModal.css';
import CancelIcon from '@material-ui/icons/Cancel';
import GroupIcon from '@material-ui/icons/Group';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import PhotoIcon from '@material-ui/icons/Photo';
import Loading from '../../Loading/Loading';
import {IconButton, ClickAwayListener, Avatar} from '@material-ui/core';
import {PostListContext} from '../../../providers/PostListContextProvider';
import {CurrentUserContext} from '../../../providers/CurrentUserContextProvider';
import {storage} from '../../../firebase';

function CreatePostModal({modalShow, setModalShow}) {
  const {setPostList, setPostListLoading} = useContext(PostListContext);
  const {getCurrentUser} = useContext(CurrentUserContext);
  const currentUser = getCurrentUser();
  const [caption, setCaption] = useState('');
  const [mediaList, setMediaList] = useState([]);
  const [uploading, setUploading] = useState(false);

  const onFileChange = (e) => {
    for (let i = 0; i < e.target.files.length; i++) {
      const mediaFile = e.target.files[i];
      mediaFile['id'] = Math.random();
      var previewMedia = URL.createObjectURL(mediaFile);

      setMediaList((prevState) => [
        ...prevState,
        {mediaFile, previewMedia, type: mediaFile.type},
      ]);
    }
  };

  const handleCreatePost = async (e) => {
    e.preventDefault();
    setUploading(true);

    const imgFileList = mediaList.map(({mediaFile}) => mediaFile);

    const promises = imgFileList.map((file) => {
      let ref = storage.ref(`instagram/${file.name}`);
      return ref.put(file).then(() => ref.getDownloadURL());
    });

    Promise.all(promises)
      .then(async (uploadedMediaList) => {
        console.log(uploadedMediaList, 'all');

        try {
          const CreatePostRes = await fetch(
            'http://localhost:5001/instagram-clone-86a6e/asia-east2/api/post',
            {
              method: 'POST',
              headers: {
                'content-type': 'application/json; charset=UTF-8',
              },

              body: JSON.stringify({
                caption,
                uploadedMediaList: uploadedMediaList.map((media, index) => {
                  return {mediaURL: media, type: mediaList[index].type};
                }),
                displayName: currentUser.displayName,
                url: currentUser.url,
                uid: currentUser.uid,
              }),
            }
          );

          const CreatePostJson = await CreatePostRes.json();
          setUploading(false);
          if (CreatePostJson) {
            try {
              const getPostListReq = await fetch(
                `https://asia-east2-instagram-clone-86a6e.cloudfunctions.net/api/post`
              );

              const getPostListJson = await getPostListReq.json();

              if (getPostListJson) {
                setPostList(getPostListJson.postList);
                setPostListLoading(false);
              } else {
                console.error(
                  getPostListJson + 'error in getPostListJson after createPost'
                );
              }
            } catch (err) {
              console.error(
                'err in trycatch block of GetPostList' + JSON.stringify(err)
              );
            }
            setModalShow(false);
            setCaption('');
            setMediaList([]);
          }
        } catch (err) {
          setUploading(false);
          console.error(
            JSON.stringify(err) + ' err in trycatch CreatePostModal'
          );
        }
      })
      .catch((err) => {
        setUploading(false);
        alert(err.code);
      });
  };

  return (
    modalShow && (
      <ClickAwayListener onClickAway={() => setModalShow(false)}>
        <div className="create__post__modal">
          <div className="create__post__modal__header">
            <div></div>
            <h5>Tạo bài viết</h5>
            <IconButton onClick={() => setModalShow(false)}>
              <CancelIcon fontSize="large" />
            </IconButton>
          </div>

          <div className="create__post__modal__avatar">
            <Avatar src={currentUser.url} />

            <div>
              <p>{currentUser.displayName}</p>
              <div>
                <GroupIcon fontSize="small" />
                <span>&nbsp; Bạn bè &nbsp;</span>
                <ArrowDropDownIcon fontSize="small" />
              </div>
            </div>
          </div>
          {uploading ? (
            <Loading />
          ) : (
            <>
              <textarea
                cols="30"
                rows="6"
                placeholder={`${currentUser.displayName} ơi, bạn đang nghĩ gì thế ?`}
                onChange={(e) => setCaption(e.target.value)}
                value={caption}
              />

              <div className="create__post__modal__preview__img__list">
                {mediaList.map(({previewMedia, type}, index) => (
                  <div
                    className="create__post__modal_preview__media__container"
                    key={index}
                  >
                    <IconButton
                      onClick={() =>
                        setMediaList(mediaList.filter((__, i) => i !== index))
                      }
                    >
                      <CancelIcon fontSize="large" />
                    </IconButton>

                    {type.includes('image') && (
                      <img src={previewMedia} alt="" />
                    )}
                    {type.includes('video') && (
                      <video src={previewMedia} controls autoPlay alt="" />
                    )}
                  </div>
                ))}
              </div>

              <div className="create__post__modal__add__img__btn">
                <span>Thêm vào bài viết</span>

                <form enctype="multipart/form-data">
                  <input
                    multiple
                    id="icon-button-file"
                    type="file"
                    accept="video/*,image/*"
                    onChange={onFileChange}
                  />

                  <label htmlFor="icon-button-file">
                    <IconButton
                      color="primary"
                      aria-label="upload picture"
                      component="span"
                    >
                      <PhotoIcon fontSize="large" />
                    </IconButton>
                  </label>
                </form>
              </div>
            </>
          )}

          <button
            disabled={uploading ? true : false}
            style={{backgroundColor: uploading ? 'lightgrey' : '#1877f2'}}
            onClick={handleCreatePost}
          >
            Đăng
          </button>
        </div>
      </ClickAwayListener>
    )
  );
}

export default CreatePostModal;
