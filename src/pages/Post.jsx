import React, {useEffect, useContext, useState, useRef} from 'react';
import './Post.css';
import {HeartIcon, SmileIcon, MessageIcon, SendIcon} from '../assets/imgs';
import Header from '../components/Header/Header';
import {useParams} from 'react-router-dom';
import Loading from '../components/Loading/Loading';
import {Carousel} from 'react-responsive-carousel';
import PostOptionBtnList from '../components/Home/PostOptionBtnList';
import {Col, Overlay, Tooltip} from 'react-bootstrap';
import prettyMilliseconds from 'pretty-ms';
import {IconButton} from '@material-ui/core';
import TextareaAutosize from 'react-textarea-autosize';
import Picker from 'emoji-picker-react';
import LikeButton from '../components/Home/LikeButton';
import {PostListContext} from '../providers/PostListContextProvider';
import CreateCommentModal from '../components/Home/PostModals/CreateCommentModal';

function Post() {
  const {postId} = useParams();
  const [postData, setPostData] = useState({});
  const [postDataLoading, setPostDataLoading] = useState(true);
  const {postList, getPostListLoading} = useContext(PostListContext);
  var createPostTimestamp = new Date(postData.createAt).getTime();
  var timeDiff = Date.now() - createPostTimestamp;
  const [comment, setComment] = useState('');
  const [commentList, setCommentList] = useState([]);

  useEffect(() => {
    if (!getPostListLoading) {
      const currentPost = postList.filter((post) => post.id === postId)[0];
      setPostData(currentPost.data);
      setCommentList(currentPost.data.commentList);
      setPostDataLoading(false);
    }
  }, [setPostData, setPostDataLoading, postList, getPostListLoading, postId]);

  useEffect(() => {
    if (!getPostListLoading && !postDataLoading) {
      setCommentList(postData.commentList);
    }
  }, [getPostListLoading, postDataLoading, setCommentList, postData]);

  return (
    <div className="Post">
      <Header />
      {getPostListLoading || !postData || postDataLoading ? (
        <Loading />
      ) : (
        <div className="Post__body__container">
          <Col
            xs={12}
            sm={7}
            className="Post__body__container__media__list__container"
          >
            <Carousel>
              {postData.uploadedMediaList.map(({mediaURL, type}, index) => (
                <div key={index}>
                  {type.includes('image') && (
                    <div
                      className="media"
                      style={{backgroundImage: `url("${mediaURL}")`}}
                    ></div>
                  )}
                  {type.includes('video') && (
                    <video className="media" src={mediaURL} controls alt="" />
                  )}
                </div>
              ))}
            </Carousel>
          </Col>
          <Col
            xs={12}
            sm={5}
            className="Post__body__container__comment__list__container"
          >
            <div className="Post__body__container__comment__list__container__post__header__container">
              <div className="Post__body__container__comment__list__container__post__header__poster__info">
                <img src={postData.url} alt="" />

                <span>{postData.displayName}</span>
              </div>

              <PostOptionBtnList data={postData} id={postId} />
            </div>

            <div className="Post__body__container__comment__list__container__post__body__container">
              <div className="Post__body__container__comment__list__container__post__body__container__post__caption__container">
                <img src={postData.url} alt="" />
                <div>
                  <span>{postData.displayName} &nbsp;</span>
                  <span>{postData.caption}</span>
                </div>
              </div>
              {commentList
                .map((comment, index) => (
                  <div
                    className="Post__body__container__comment__list__container__post__body__container__post__comment__container"
                    key={index}
                  >
                    <img src={comment.url} alt="" />
                    <div>
                      <div>
                        <span>{comment.displayName}&nbsp;</span>
                        <span>{comment.comment}</span>
                      </div>
                      <div>
                        <span>
                          {prettyMilliseconds(timeDiff, {compact: true})}
                        </span>

                        <button>
                          {comment.likedCommentList.length} lượt thích
                        </button>

                        <button>Trả lời</button>
                      </div>
                    </div>
                    <img src={HeartIcon} alt="" />
                  </div>
                ))
                .reverse()}
            </div>

            <div className="Post__body__container__comment__list__container__post__footer__container">
              <div className="Post__body__container__comment__list__container__post__footer__container__option__btn__list__container">
                <div>
                  <LikeButton data={postData} id={postId} />
                  <IconButton>
                    <img src={MessageIcon} alt="" />
                  </IconButton>
                  <IconButton>
                    <img src={SendIcon} alt="" />
                  </IconButton>
                </div>

                <p>
                  {postData.likedList?.length === 0
                    ? `${postData.like} lượt thích`
                    : `${postData.likedList
                        .map((each) => each.displayName)
                        .join(', ')} và ${postData.like} người khác đã thích`}
                </p>

                <p>{prettyMilliseconds(timeDiff, {compact: true})}</p>
              </div>
              <CreateCommentModal
                comment={comment}
                setComment={setComment}
                postId={postId}
                setCommentList={setCommentList}
              />
            </div>
          </Col>
        </div>
      )}
    </div>
  );
}

export default Post;
