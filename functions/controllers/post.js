const {db} = require('../util/admin');
const FieldValue = require('firebase-admin').firestore.FieldValue;

exports.get__all__post = (req, res, next) => {
  return db
    .collection('instagram__posts')
    .orderBy('createAt', 'desc')
    .onSnapshot((snapshot) => {
      return res.status(200).json({
        postList: snapshot.docs.map((doc) => ({
          id: doc.id,
          data: doc.data(),
        })),
      });
    });
};

exports.get__post__by__id = (req, res, next) => {
  const {id} = req.params;

  return db
    .collection('instagram__posts')
    .doc(id)
    .get()
    .then((doc) => res.status(200).json({data: doc.data(), message: 'get doc successfully'}))
    .catch((err) => {
      console.error(err);
      return res.status(500).json(err);
    });
};
exports.create__post = (req, res, next) => {
  const {caption, uploadedMediaList, url, uid, displayName} = req.body;

  return db
    .collection('instagram__posts')
    .add({
      caption,
      uploadedMediaList,
      url,
      uid,
      displayName,
      createAt: new Date().toISOString(),
      commentList: [],
      like: Math.floor(Math.random() * (100000 - 10 + 1)) + 10,
      likedList: [],
    })
    .then((doc) => {
      res.status(200).json({message: 'new post created'});
    })
    .catch((err) => {
      res.status(500).json({err});
      console.log(err);
    });
};

exports.edit__post = (req, res, next) => {
  const {id} = req.params;
  const {
    editedCaption,
    uploadedAdditionalMediaList,
    existedMediaList,
  } = req.body;

  return db
    .collection('instagram__posts')
    .doc(id)
    .update({
      caption: editedCaption,
      uploadedMediaList: [...uploadedAdditionalMediaList, ...existedMediaList],
    })
    .then((result) => {
      console.log(result, 'result');
      return res.status(200).json({message: 'post edited', result});
    })
    .catch((err) => {
      return res.status(500).json({message: 'err in editing post', err});
    });
};

exports.delete__post = (req, res, next) => {
  const {id} = req.params;

  return db
    .collection('instagram__posts')
    .doc(id)
    .delete()
    .then((result) => {
      return res.status(200).json({message: 'post deleted', result});
    })
    .catch((err) => {
      return res.status(500).json({message: 'err in deleting post', err});
    });
};

exports.like__post = (req, res, next) => {
  const {id} = req.params;
  const {uid, displayName} = req.body;

  return db
    .collection('instagram__posts')
    .doc(id)
    .update({
      likedList: FieldValue.arrayUnion({
        uid,
        displayName,
      }),
    })
    .then((doc) => {
      console.log(`${displayName} liked post at id ${id}`, displayName);
      return res.status(200).json({liked: true});
    })
    .catch((err) => {
      res.status(500).json({err});
      console.log(err);
    });
};

exports.get__updated__likedList = async (req, res, next) => {
  const {id} = req.params;
  const {uid} = req.body;

  try {
    db.collection('instagram__posts')
      .doc(id)
      .onSnapshot(async (snapshot) => {
        var updatedLikedList = await snapshot
          .data()
          .likedList.filter((each) => {
            return each.uid !== uid;
          });
        req.updatedLikedList = updatedLikedList;
        next();
      });
  } catch (err) {
    res.status(500).json({err});
    console.log(err);
  }
};

exports.unlike__post = async (req, res, next) => {
  const {id} = req.params;
  const {uid} = req.body;

  let writeResult = await db
    .collection('instagram__posts')
    .doc(id)
    .set({likedList: req.updatedLikedList}, {merge: true});

  if (writeResult) {
    console.log(
      `unliked post with id ${id} by user with uid ${uid}`,
      writeResult
    );

    next();
  }
};

exports.return__like__list = (req, res, next) => {
  console.log('return new likeList', req.updatedLikedList);
  return res
    .status(200)
    .json({unliked: true, updatedLikedList: req.updatedLikedList});
};
