import React, {useState, useContext} from 'react';
import {CurrentUserContext} from '../../providers/CurrentUserContextProvider';
import SearchIcon from '@material-ui/icons/Search';
import CancelIcon from '@material-ui/icons/Cancel';
import {useHistory} from 'react-router-dom';
import {
  SendIcon,
  HomeIcon,
  HeartIcon,
  CompassIcon,
  LogoIcon,
} from '../../assets/imgs';
import {IconButton, ClickAwayListener} from '@material-ui/core';

import {Row, Col} from 'react-bootstrap';
import './Header.css';

function Header() {
  const [visibleInput, setVisibleInput] = useState(false);
  const {getCurrentUser} = useContext(CurrentUserContext);
  const history = useHistory();
  const currentUser = getCurrentUser();

  return (
    <div className="container-fluid">
      <Row className="header">
        <Col
          xs={12}
          sm={4}
          className="header__logo"
          onClick={() => history.push('/')}
        >
          <img className="app__headerImage" src={LogoIcon} alt="" />
        </Col>
        <Col xs={12} sm={4} className="header__input__container">
          {visibleInput && (
            <ClickAwayListener onClickAway={() => setVisibleInput(false)}>
              <div>
                <SearchIcon />
                <input type="text" placeholder="Tim kiem" autoFocus />
                <CancelIcon onClick={() => setVisibleInput(!visibleInput)} />
              </div>
            </ClickAwayListener>
          )}

          {!visibleInput && (
            <button onClick={() => setVisibleInput(!visibleInput)}>
              <SearchIcon />
              <span>Tim kiem</span>
            </button>
          )}
        </Col>
        <Col xs={12} sm={4} className="header__option__buttons">
          <IconButton>
            <img src={HomeIcon} alt="HomeIcon" />
          </IconButton>

          <IconButton>
            <img src={SendIcon} alt="SendIcon" />
          </IconButton>

          <IconButton>
            <img src={CompassIcon} alt="CompassIcon" />
          </IconButton>

          <IconButton>
            <img src={HeartIcon} alt="HeartIcon" />
          </IconButton>

          {currentUser && (
            <IconButton>
              <img src={currentUser.url} alt="avatar" />
            </IconButton>
          )}
        </Col>
      </Row>
    </div>
  );
}

export default Header;
