import React, {useState, useRef, useContext} from 'react';
import {SmileIcon} from '../../../assets/imgs';
import {IconButton} from '@material-ui/core';
import {Overlay, Tooltip} from 'react-bootstrap';
import {CurrentUserContext} from '../../../providers/CurrentUserContextProvider';

import TextareaAutosize from 'react-textarea-autosize';
import Picker from 'emoji-picker-react';
import './CreateCommentModal.css';

function CreateCommentModal({comment, setComment, postId, setCommentList}) {
  const target = useRef(null);
  const [show, setShow] = useState(false);
  const {getCurrentUser} = useContext(CurrentUserContext);
  const {displayName, url, uid} = getCurrentUser();

  const handleCreateNewComment = async (e) => {
    e.preventDefault();

    if (e.keyCode === 13 && comment !== '') {
      setComment('');
      try {
        const AddCommentRes = await fetch(
          `http://localhost:5001/instagram-clone-86a6e/asia-east2/api/comment/${postId}`,
          {
            method: 'POST',
            headers: {
              'content-type': 'application/json; charset=UTF-8',
            },

            body: JSON.stringify({
              comment,
              displayName,
              uid,
              url,
            }),
          }
        );

        const AddCommentJson = await AddCommentRes.json();

        if (AddCommentJson.updatedCommentList) {
          setCommentList(AddCommentJson.updatedCommentList);
        }
      } catch (err) {
        console.error(JSON.stringify(err) + ' err in trycatch AddCommentRes');
      }
    }
  };

  return (
    <div className="create__comment__modal">
      <IconButton ref={target} onClick={() => setShow(!show)}>
        <img src={SmileIcon} alt="" />
      </IconButton>

      <Overlay target={target.current} show={show} placement="top">
        {(props) => (
          <Tooltip {...props}>
            <Picker
              onEmojiClick={(event, emojiObject) =>
                setComment(comment + ' ' + emojiObject.emoji)
              }
            />
          </Tooltip>
        )}
      </Overlay>
      <TextareaAutosize
        className="post__comment__area"
        minRows={1}
        maxRows={1000000}
        placeholder="Thêm bình luận..."
        value={comment}
        onChange={(e) => setComment(e.target.value)}
        onKeyUp={handleCreateNewComment}
      />
      <span onClick={handleCreateNewComment}>Đăng</span>
    </div>
  );
}

export default CreateCommentModal;
