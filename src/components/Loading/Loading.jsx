import React from 'react';
import {LoadingGif} from '../../assets/imgs';
import './Loading.css';

function Loading() {
  return (
    <img
      className="create__post__modal__uploading__gif"
      src={LoadingGif}
      alt=""
    />
  );
}

export default Loading;
