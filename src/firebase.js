import firebase from 'firebase';

const firebaseApp = firebase.initializeApp({
  apiKey: 'AIzaSyApwBAFrEI7qALgn0fLM4ODtxUVRBvHerY',
  authDomain: 'instagram-clone-86a6e.firebaseapp.com',
  databaseURL: 'https://instagram-clone-86a6e.firebaseio.com',
  projectId: 'instagram-clone-86a6e',
  storageBucket: 'instagram-clone-86a6e.appspot.com',
  messagingSenderId: '365424826617',
  appId: '1:365424826617:web:8a310cfa020156769f2a28',
});

const db = firebaseApp.firestore();
const auth = firebaseApp.auth();
const storage = firebaseApp.storage();
const provider = new firebase.auth.FacebookAuthProvider();

export {db, auth, storage, provider};
