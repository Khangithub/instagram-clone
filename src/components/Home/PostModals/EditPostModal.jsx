import React, {useContext, useState, useRef} from 'react';
import {Modal, Overlay, Tooltip} from 'react-bootstrap';
import GroupIcon from '@material-ui/icons/Group';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import {IconButton, Avatar} from '@material-ui/core';
import CancelIcon from '@material-ui/icons/Cancel';
import PhotoIcon from '@material-ui/icons/Photo';
import {CurrentUserContext} from '../../../providers/CurrentUserContextProvider';
import {PostListContext} from '../../../providers/PostListContextProvider';
import {storage} from '../../../firebase';
import Loading from '../../Loading/Loading';
import {SmileIcon} from '../../../assets/imgs';
import Picker from 'emoji-picker-react';

import './EditPostModal.css';

function EditPostModal(props) {
  const {currentUser} = useContext(CurrentUserContext);
  var {data, id, setModalShow} = props;
  const [show, setShow] = useState(false);
  const target = useRef(null);
  const [editedCaption, setEditedCaption] = useState(data.caption);
  const [existedMediaList, setExistedMediaList] = useState(
    data.uploadedMediaList
  );

  const [additionalMediaList, setAdditionalMediaList] = useState([]);
  const {setPostList, setPostListLoading} = useContext(PostListContext);

  const [uploading, setUploading] = useState(false);

  const onFileChange = (e) => {
    for (let i = 0; i < e.target.files.length; i++) {
      const mediaFile = e.target.files[i];
      mediaFile['id'] = Math.random();
      var previewMedia = URL.createObjectURL(mediaFile);

      setAdditionalMediaList((prevState) => [
        ...prevState,
        {mediaFile, previewMedia, type: mediaFile.type},
      ]);
    }
  };

  const handleEditPost = async (e) => {
    e.preventDefault();
    setUploading(true);

    const imgFileList = additionalMediaList.map(({mediaFile}) => mediaFile);

    const promises = imgFileList.map((file) => {
      let ref = storage.ref(`instagram/${file.name}`);
      return ref.put(file).then(() => ref.getDownloadURL());
    });

    Promise.all(promises)
      .then(async (uploadedAdditionalMediaList) => {
        console.log(uploadedAdditionalMediaList, 'additional');

        try {
          const CreatePostRes = await fetch(
            `http://localhost:5001/instagram-clone-86a6e/asia-east2/api/post/${id}`,
            {
              method: 'PATCH',
              headers: {
                'content-type': 'application/json; charset=UTF-8',
              },

              body: JSON.stringify({
                editedCaption,
                uploadedAdditionalMediaList: uploadedAdditionalMediaList.map(
                  (media, index) => {
                    return {
                      mediaURL: media,
                      type: additionalMediaList[index].type,
                    };
                  }
                ),
                existedMediaList,
              }),
            }
          );

          const EditPostJson = await CreatePostRes.json();
          setUploading(false);
          if (EditPostJson) {
            try {
              const getPostListReq = await fetch(
                `https://asia-east2-instagram-clone-86a6e.cloudfunctions.net/api/post`
              );

              const getPostListJson = await getPostListReq.json();

              if (getPostListJson) {
                setPostList(getPostListJson.postList);
                setPostListLoading(false);
              } else {
                console.error(
                  getPostListJson + 'error in getPostListJson after editPost'
                );
              }
            } catch (err) {
              console.error(
                'err in trycatch block of EditPostList' + JSON.stringify(err)
              );
            }
            setModalShow(false);
          }
        } catch (err) {
          setUploading(false);
          console.error(JSON.stringify(err) + ' err in trycatch EditPostModal');
        }
      })
      .catch((err) => {
        setUploading(false);
        alert(err.code);
      });
  };

  return (
    <Modal
      className="edit__post__modal"
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <div className="edit__post__modal__header">
        <div></div>
        <p>Edit Post</p>
        <IconButton onClick={props.onHide}>
          <CancelIcon fontSize="large" />
        </IconButton>
      </div>
      <div className="edit__post__modal__body">
        <div className="edit__post__modal__post__user__info">
          <Avatar src={currentUser.url} />

          <div>
            <p>{currentUser.displayName}</p>
            <div>
              <GroupIcon fontSize="small" />
              <span>&nbsp; Bạn bè &nbsp;</span>
              <ArrowDropDownIcon fontSize="small" />
            </div>
          </div>
        </div>

        {uploading ? (
          <Loading />
        ) : (
          <>
            <textarea
              className="edit__post__modal__caption"
              cols="30"
              rows="6"
              value={editedCaption}
              onChange={(e) => setEditedCaption(e.target.value)}
            ></textarea>
            <div className="edit__post__modal__emoji__picker__container">
              <IconButton ref={target} onClick={() => setShow(!show)}>
                <img src={SmileIcon} alt="" />
              </IconButton>{' '}
              <Overlay target={target.current} show={show} placement="top">
                {(props) => (
                  <Tooltip
                    className="Post__body__container__comment__list__container__post__footer__container__add__comment__container__emoji__picker"
                    {...props}
                  >
                    <Picker
                      onEmojiClick={(event, emojiObject) =>
                        setEditedCaption(
                          editedCaption + ' ' + emojiObject.emoji
                        )
                      }
                    />
                  </Tooltip>
                )}
              </Overlay>
            </div>

            <div className="edit__post__modal__body__media__list__container">
              {additionalMediaList.map(({previewMedia, type}, index) => (
                <div
                  className="edited__post__modal_preview__img__container"
                  key={index}
                >
                  <IconButton
                    onClick={() =>
                      setAdditionalMediaList(
                        additionalMediaList.filter((__, i) => i !== index)
                      )
                    }
                  >
                    <CancelIcon fontSize="large" />
                  </IconButton>

                  {type.includes('image') && <img src={previewMedia} alt="" />}
                  {type.includes('video') && (
                    <video src={previewMedia} controls autoPlay alt="" />
                  )}
                </div>
              ))}
              {existedMediaList.map(({mediaURL, type}, index) => (
                <div
                  className="edited__post__modal_preview__img__container"
                  key={index}
                >
                  <IconButton
                    onClick={() =>
                      setExistedMediaList(
                        existedMediaList.filter((__, i) => i !== index)
                      )
                    }
                  >
                    <CancelIcon fontSize="large" />
                  </IconButton>

                  {type.includes('image') && <img src={mediaURL} alt="" />}
                  {type.includes('video') && (
                    <video src={mediaURL} controls autoPlay alt="" />
                  )}
                </div>
              ))}
            </div>

            <div className="edit__post__modal__add__img__btn">
              <span>Chỉnh sửa bài viết</span>

              <form enctype="multipart/form-data">
                <input
                  multiple
                  id="icon-button-file"
                  type="file"
                  accept="video/*,image/*"
                  onChange={onFileChange}
                />

                <label htmlFor="icon-button-file">
                  <IconButton
                    color="primary"
                    aria-label="upload picture"
                    component="span"
                  >
                    <PhotoIcon fontSize="large" />
                  </IconButton>
                </label>
              </form>
            </div>
          </>
        )}
      </div>

      <div className="edit__post__modal__footer">
        <button
          disabled={uploading ? true : false}
          style={{backgroundColor: uploading ? 'lightgrey' : '#1877f2'}}
          onClick={handleEditPost}
        >
          Chỉnh sửa
        </button>
      </div>
    </Modal>
  );
}

export default EditPostModal;
