import React from 'react';
import './SubStory.css';
import {useHistory} from 'react-router-dom';

export default function SubStory({subStory, subStoryIndex}) {
  const history = useHistory();
  const {created, username, avatar, thumbnailURL} = subStory;

  return (
    <div
      className="sub__story anime"
      onClick={() => history.push(`/stories/${subStoryIndex}`)}
    >
      <div
        className="sub__story__bg"
        style={{backgroundImage: `url("${thumbnailURL}")`}}
      ></div>

      <div className="sub__story__user__info">
        <img src={avatar} alt="" draggable={false} />
        <p>{username}</p>
        <p>{created} giờ</p>
      </div>
    </div>
  );
}
