import React, {useState, useContext} from 'react';
import {PostListContext} from '../../providers/PostListContextProvider';
import './Feed.css';
import {IconButton} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import StoryList from './StoryList';
import CreatePostModal from './PostModals/CreatePostModal';
import Post from './Post';
import Loading from '../Loading/Loading';

function Feed() {
  const {postList, getPostListLoading} = useContext(PostListContext);
  const [modalShow, setModalShow] = useState(false);

  return (
    <div className="feed">
      <StoryList />
      <CreatePostModal modalShow={modalShow} setModalShow={setModalShow} />

      <div className="feed__post__list__container">
        {getPostListLoading ? (
          <Loading />
        ) : (
          postList.map(({data, id}) => <Post key={id} data={data} id={id} />)
        )}
      </div>

      <IconButton
        onClick={() => {
          document.body.scrollTop = 0;
          document.documentElement.scrollTop = 0;

          setModalShow(true);
        }}
      >
        <AddIcon fontSize="large" />
      </IconButton>
    </div>
  );
}

export default Feed;
